package com.kfk.ms.kfkconsumer.listener;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaConsumer {
	
	@KafkaListener(topics="topicName")
	public void consumeMsg(String meassage) {
		System.out.println("KafkaConsumer.consumeMsg()_ Message: " + meassage);
	}

}
